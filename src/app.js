const app = require('express')();
const http = require('http').Server(app)
const io = require('socket.io')(http, {
    pingInterval: 1000
});

const MAX_PLAYERS = 2;
const CLIENT_CONFIG = {
    maxPlayers: MAX_PLAYERS
};
// array with room IDs as name
// has values:
// rooms.playersInRoom: int
const rooms = {};
const ROOM_TEMPLATE = Object.freeze({
    playersInRoom: 0
});

// parse arguments
var argv = require('minimist')(process.argv.slice(2));
const port = argv.port || process.env.PORT || (process.env.NODE_ENV === 'production' ? 443 : 4000);

// listen server
http.listen(port, () => {
    console.log('Listening on port ' + port);
});

// server HTML fallback
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// handle WebSocket/SocketIo
io.on('connection', socket => {
    let previousRoomId;

    // common
    function sendError(message) {
        console.error('Error to ', socket.id, ':', message);
        socket.emit('errorMessage', message);
    }

    // game management
    function safeJoin(roomId) {
        if (isInRoom(roomId, socket.id)) {
            sendError(`You are already in this room!`);
            return;
        }
        if (getPlayersCountInRoom(roomId) >= MAX_PLAYERS) {
            sendError(`This room ${roomId} is full!`);
            return;
        }

        if (previousRoomId) {
            leaveRoom(previousRoomId);
        }

        socket.join(roomId, (err) => {
            if (err) {
                sendError(`Joining room ${roomId} failed with message "${err}".`);
                return;
            } else {
                console.log(`Socket ${socket.id} joined room ${roomId}`);
                previousRoomId = roomId;
                // confirm joined room to client
                socket.emit('joinedRoom', rooms[roomId]);
            }

            broadcastRoomStates();

            // game logic
            startGameIfRoomWants(roomId);
        });
    }

    // Room management
    function updateInternalRoomState() {
        for (const roomId in rooms) {
            rooms[roomId].playersInRoom = getPlayersCountInRoom(roomId);
        }
    }
    function getPlayersCountInRoom(roomId) {
        // if room is not yet created
        if (!io.sockets.adapter.rooms[roomId]) {
            return 0;
        }

        // https://stackoverflow.com/questions/23045245/one-line-check-if-socket-is-in-given-room
        return Object.keys(io.sockets.adapter.rooms[roomId].sockets).length || 0;
    }
    function isInRoom(roomId, socketId = socket.id) {
        // if room is not yet created
        if (!io.sockets.adapter.rooms[roomId]) {
            return false;
        }

        // https://stackoverflow.com/questions/23045245/one-line-check-if-socket-is-in-given-room
        return io.sockets.adapter.rooms[roomId].sockets[socketId];
    }
    // find room of socket
    function getRoomOfSocket(opt = {}) {
        opt.socketId = opt.socketId || socket.id;
        opt.failSilently = opt.failSilently || false;

        const roomId = Object.keys(io.sockets.sockets[opt.socketId].rooms).filter(item => item != opt.socketId)[0];
        if (!opt.failSilently && !roomId) {
            sendError('You are not in any multiplayer room. This is a fatal error that may cause game errors. We apologize for interruping your game play.')
        }

        return roomId;
    }

    function broadcastRoomStates() {
        updateInternalRoomState();
        // broadcast + ourselve
        io.emit('rooms', Object.values(rooms));

        io.emit('clientConfig', CLIENT_CONFIG);
    }
    function updateRoomStatesForSender() {
        updateInternalRoomState();
        socket.emit('rooms', Object.values(rooms));

        io.emit('clientConfig', CLIENT_CONFIG);
    }
    function joinRoom(roomId) {
        safeJoin(roomId);
    }
    function leaveRoom(roomId) {
        socket.leave(roomId);

        // update state for all clients
        broadcastRoomStates();
    }

    socket.on('getRoomsStates', () => {
        updateRoomStatesForSender();
    });

    socket.on('joinRoom', roomId => {
        joinRoom(roomId);
    });

    socket.on('addRoom', room => {
        const roomId = room.id;

        if (Object.keys(rooms).includes(roomId)) {
            sendError('Room does already exist!');
            return;
        }

        // new room
        rooms[roomId] = {};
        Object.assign(rooms[roomId], ROOM_TEMPLATE);
        rooms[roomId].id = roomId;

        broadcastRoomStates();
        console.log('room added:', room, '; all rooms:', rooms);

        // note: also automatically joins (may not be wanted for us)
        joinRoom(roomId);
    });

    // game logic
    function startGameIfRoomWants(roomId) {
        const playerCount = getPlayersCountInRoom(roomId);
        console.log(`${playerCount} players in ${roomId}.`);

        if (playerCount == 1) {
            // this player is the first player
            socket.emit('firstPlayer', true);
            console.log(`firstPlayer selected: ${socket.id} in ${roomId}.`);
        }

        if (playerCount == 2) {
            // start the game
            io.in(roomId).emit('startGame');
            console.log(`startGame in ${roomId}.`);
        }
    }

    // just relay 'e2ePing' and 'e2ePong'
    socket.on('e2ePing', data => {
        const roomId = getRoomOfSocket({
            failSilently: true
        });
        socket.to(roomId).emit('e2ePing', data);
    });
    socket.on('e2ePong', data => {
        const roomId = getRoomOfSocket({
            failSilently: true
        });
        socket.to(roomId).emit('e2ePong', data);
    });

    // just relay 'playerChange'
    socket.on('playerChange', data => {
        const roomId = getRoomOfSocket();
        socket.to(roomId).emit('playerChange', data.playerChange);
    });

    // just relay 'tickFinished'
    socket.on('tickFinished', data => {
        const roomId = getRoomOfSocket();
        socket.to(roomId).emit('tickFinished', true);
    });


    // on connecting
    broadcastRoomStates();
    console.log(`Socket ${socket.id} has connected`);
});
